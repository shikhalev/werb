/*
 * encoding: utf-8
 *
 * This file is a part of WeRB project.
 * License: LGPLv3+
 *
 */

// PHPJS
function utf8_encode (argString) {
  // http://kevin.vanzonneveld.net
  // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +   improved by: sowberry
  // +    tweaked by: Jack
  // +   bugfixed by: Onno Marsman
  // +   improved by: Yves Sucaet
  // +   bugfixed by: Onno Marsman
  // +   bugfixed by: Ulrich
  // +   bugfixed by: Rafal Kukawski
  // +   improved by: kirilloid
  // *     example 1: utf8_encode('Kevin van Zonneveld');
  // *     returns 1: 'Kevin van Zonneveld'

  if (argString === null || typeof argString === "undefined") {
    return "";
  }

  var string = (argString + ''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
  var utftext = '',
    start, end, stringl = 0;

  start = end = 0;
  stringl = string.length;
  for (var n = 0; n < stringl; n++) {
    var c1 = string.charCodeAt(n);
    var enc = null;

    if (c1 < 128) {
      end++;
    } else if (c1 > 127 && c1 < 2048) {
      enc = String.fromCharCode((c1 >> 6) | 192, (c1 & 63) | 128);
    } else {
      enc = String.fromCharCode((c1 >> 12) | 224, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128);
    }
    if (enc !== null) {
      if (end > start) {
        utftext += string.slice(start, end);
      }
      utftext += enc;
      start = end = n + 1;
    }
  }

  if (end > start) {
    utftext += string.slice(start, stringl);
  }

  return utftext;
}

function sha1 (str) {
  // http://kevin.vanzonneveld.net
  // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
  // + namespaced by: Michael White (http://getsprink.com)
  // +      input by: Brett Zamir (http://brett-zamir.me)
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // -    depends on: utf8_encode
  // *     example 1: sha1('Kevin van Zonneveld');
  // *     returns 1: '54916d2e62f65b3afa6e192e6a601cdbe5cb5897'
  var rotate_left = function (n, s) {
    var t4 = (n << s) | (n >>> (32 - s));
    return t4;
  };

/*var lsb_hex = function (val) { // Not in use; needed?
    var str="";
    var i;
    var vh;
    var vl;

    for ( i=0; i<=6; i+=2 ) {
      vh = (val>>>(i*4+4))&0x0f;
      vl = (val>>>(i*4))&0x0f;
      str += vh.toString(16) + vl.toString(16);
    }
    return str;
  };*/

  var cvt_hex = function (val) {
    var str = "";
    var i;
    var v;

    for (i = 7; i >= 0; i--) {
      v = (val >>> (i * 4)) & 0x0f;
      str += v.toString(16);
    }
    return str;
  };

  var blockstart;
  var i, j;
  var W = new Array(80);
  var H0 = 0x67452301;
  var H1 = 0xEFCDAB89;
  var H2 = 0x98BADCFE;
  var H3 = 0x10325476;
  var H4 = 0xC3D2E1F0;
  var A, B, C, D, E;
  var temp;

  str = this.utf8_encode(str);
  var str_len = str.length;

  var word_array = [];
  for (i = 0; i < str_len - 3; i += 4) {
    j = str.charCodeAt(i) << 24 | str.charCodeAt(i + 1) << 16 | str.charCodeAt(i + 2) << 8 | str.charCodeAt(i + 3);
    word_array.push(j);
  }

  switch (str_len % 4) {
  case 0:
    i = 0x080000000;
    break;
  case 1:
    i = str.charCodeAt(str_len - 1) << 24 | 0x0800000;
    break;
  case 2:
    i = str.charCodeAt(str_len - 2) << 24 | str.charCodeAt(str_len - 1) << 16 | 0x08000;
    break;
  case 3:
    i = str.charCodeAt(str_len - 3) << 24 | str.charCodeAt(str_len - 2) << 16 | str.charCodeAt(str_len - 1) << 8 | 0x80;
    break;
  }

  word_array.push(i);

  while ((word_array.length % 16) != 14) {
    word_array.push(0);
  }

  word_array.push(str_len >>> 29);
  word_array.push((str_len << 3) & 0x0ffffffff);

  for (blockstart = 0; blockstart < word_array.length; blockstart += 16) {
    for (i = 0; i < 16; i++) {
      W[i] = word_array[blockstart + i];
    }
    for (i = 16; i <= 79; i++) {
      W[i] = rotate_left(W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16], 1);
    }


    A = H0;
    B = H1;
    C = H2;
    D = H3;
    E = H4;

    for (i = 0; i <= 19; i++) {
      temp = (rotate_left(A, 5) + ((B & C) | (~B & D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
      E = D;
      D = C;
      C = rotate_left(B, 30);
      B = A;
      A = temp;
    }

    for (i = 20; i <= 39; i++) {
      temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
      E = D;
      D = C;
      C = rotate_left(B, 30);
      B = A;
      A = temp;
    }

    for (i = 40; i <= 59; i++) {
      temp = (rotate_left(A, 5) + ((B & C) | (B & D) | (C & D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
      E = D;
      D = C;
      C = rotate_left(B, 30);
      B = A;
      A = temp;
    }

    for (i = 60; i <= 79; i++) {
      temp = (rotate_left(A, 5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
      E = D;
      D = C;
      C = rotate_left(B, 30);
      B = A;
      A = temp;
    }

    H0 = (H0 + A) & 0x0ffffffff;
    H1 = (H1 + B) & 0x0ffffffff;
    H2 = (H2 + C) & 0x0ffffffff;
    H3 = (H3 + D) & 0x0ffffffff;
    H4 = (H4 + E) & 0x0ffffffff;
  }

  temp = cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4);
  return temp.toLowerCase();
}
// END PHPJS

// MONKEY-PATCHING, no way...
Date.prototype.toJSON = function() {
  var year = this.getFullYear();
  var month = this.getMonth() + 1;
  var day = this.getDate();
  var hours = this.getHours();
  var minutes = this.getMinutes();
  var seconds = this.getSeconds();
  var ms = this.getMilliseconds();
  var offset = this.getTimezoneOffset();

  var result = year.toString();
  result += '-';
  if (month < 10) {
    result += '0';
  }
  result += month.toString();
  result += '-';
  if (day < 10) {
    result += '0';
  }
  result += day.toString();
  result += 'T';
  if (hours < 10) {
    result += '0';
  }
  result += hours.toString();
  result += ':';
  if (minutes < 10) {
    result += '0';
  }
  result += minutes.toString();
  result += ':';
  if (seconds < 10) {
    result += '0';
  }
  result += seconds.toString();
  result += '.';
  if (ms < 100) {
    result += '0';
    if (ms < 10) {
      result += '0';
    }
  }
  result += ms.toString();
  if (offset === 0) {
    result += 'Z';
  } else {
    if (offset < 0) {
      result += '+';
      offset = -offset;
    } else {
      result += '-';
    }
    var tzhours = Math.floor(offset / 60);
    var tzminutes = offset % 60;
    if (tzhours < 10) {
      result += '0';
    }
    result += tzhours.toString();
    result += ':';
    if (tzminutes < 10) {
      result += '0'
    }
    result += tzminutes.toString();
  }
  return result;
}

RegExp.prototype.toJSON = function() {
  return this.toString();
}
// END MONKEY-PATCHING

var WeRB = {

  msgWait : 'Ожидание...',

  JSON : {

    INDENT : '  ',
    DATE_MASK : /^\d{4}\-\d{2}\-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3})?([+\-]\d{2}:\d{2}|Z)$/,
    REGEXP_MASK : /^\/.*\/[giomxnesu]*$/,

    stringify : function (object) {
      return window.JSON.stringify(object, null, WeRB.JSON.INDENT);
    },

    parse : function (json) {
      try {
        return window.JSON.parse(json, function (name, value) {
          if (WeRB.JSON.DATE_MASK.test(value)) {
            return new Date(value);
          } else if (WeRB.JSON.REGEXP_MASK.test(value)) {
            return eval(value);
          } else {
            return value;
          }
        });
      } catch (e) {
        return null;
      }
    }

  },

  getXHR : function () {
    if (window.XMLHttpRequest) {
      return new XMLHttpRequest();
    } else {
      try {
        return new ActiveXObject('Msxml2.XMLHTTP');
      } catch (e) {
        try {
          return new ActiveXObject('Microsoft.XMLHTTP');
        } catch (e) {
          return false;
        }
      }
    }
  },

  addEvent : function (element, event, handler, capture) {
    if (element.addEventListener) {
      element.addEventListener(event, handler, !!capture);
    } else {
      element.attachEvent('on' + event, handler);
    }
  },

  removeEvent : function (element, event, handler, capture) {
    if (element.removeEventListener) {
      element.removeEventListener(event, handler, !!capture);
    } else {
      element.detachEvent('on' + event, handler);
    }
  },

  apply : function (target, source) {
    if (target instanceof Array) {
      for (var i = 0, len = source.length; i < len; i++) {
        if (!(i in source)) {
          continue;
        }
        if (target[i] && target[i] instanceof Object && !(target instanceof Date || target instanceof RegExp)) {
          WeRB.apply(target[i], source[i]);
        } else {
          target[i] = source[i];
        }
      }
    } else {
      for (var p in source) {
        if (/^(_|\$)/.test(p)) {
          continue;
        }
        if (p === 'content') {
          for (var i = 0, len = source.content.length; i < len; i++) {
            var element = document.getElementById(source.content[i].id);
            WeRB.apply(element, source.content[i]);
          }
        } else {
          if (target[p] && target[p] instanceof Object && !(target instanceof Date || target instanceof RegExp)) {
            WeRB.apply(target[p], source[p]);
          } else {
            target[p] = source[p];
          }
        }
      }
    }
    return target;
  },

  updateIcon : function (href, type) {
    var links = document.getElementsByTagName('link');
    var element = null;
    var flag = !!window.DOMTokenList;
    for (var i = 0, len = links.length; i < len; i++) {
      if (flag && links[i].relList.contains('icon') || /(^|\s)icon(\s|$)/.test(links[i].rel)) {
        element = links[i];
        break;
      }
    }
    if (!element) {
      element = document.createElement('link');
      element.rel = 'shortcut icon';
      document.head.appendChild(element);
    }
    element.href = href;
    if (type) {
      element.type = type;
    }
  },

  updatePage : function (json) {
    try {
      var data = WeRB.JSON.parse(json);
      if (data.title) {
        document.title = data.title;
      }
      if (data.icon) {
        WeRB.updateIcon(data.icon.href, data.icon.type);
      }
      if (data.content) {
        for (var i = 0, len = data.content.length; i < len; i++) {
          var item = data.content[i];
          var element = document.getElementById(item.id);
          if (!element) {
            console.log({
              error : {
                func : 'updatePage()',
                error : 'Element not found.',
                item : item
              }
            });
            continue;
          }
          WeRB.apply(element, item);
        }
      }
      return true;
    } catch (e) {
      console.log({
        error : {
          func : 'updatePage()',
          error : e
        }
      });
      return false;
    }
  },

  newMessageId : (function () {
    var counter = 0;
    return function() {
      return 'werb-msgbox-' + (++counter).toString();
    }
  })(),

  showMessage : function (msg, timeout, cls) {
    var divs = document.getElementsByClassName('werb-messagebox');
    var element = null;
    console.log({
      message : {
        msg : msg,
        cls : cls,
        log : new Date()
      }
    });
    for (var i = 0, len = divs.length; i < len; i++) {
      if (divs[i].style.display === 'none') {
        element = divs[i];
        break;
      }
    }
    if (!element) {
      element = document.createElement('div');
      element.id = WeRB.newMessageId();
      element.className = 'werb-messagebox';
      element.style.display = 'none';
      document.body.appendChild(element);
    }
    html = '<div class="werb-messagebody">' + msg;
    if (timeout == null) {
      html += '<input class="werb-messageclose" type="button" value="Close" onclick="WeRB.hideMessage(\'' + element.id + '\');" />';
      WeRB.addClass(element, 'closeable');
    }
    html += '</div>';
    element.innerHTML = html;
    if (cls) {
      WeRB.addClass(element, cls);
    }
    element.style.position = 'absolute';
    element.style.display = 'block';
    console.log({
      ww : window.innerHeight,
      wh : window.innerWidth,
      ew : element.offsetWidth,
      eh : element.offsetHeight
    });
    element.style.left = ((window.innerWidth - element.offsetWidth) / 2) + 'px';
    element.style.top = ((window.innerHeight - element.offsetHeight) / 2) + 'px';
    if (timeout !== 0) {
      setTimeout(function () {
        WeRB.hideMessage(element);
      }, timeout);
    }
    return element;
  },

  hideMessage : function (object) {
    // alert(object);
    if (typeof object === 'string') {
      object = document.getElementById(object);
    }
    object.style.display = 'none';
  },

  clickHandler : function (event) {
    event = event || window.event;
    var element = event.target || event.srcElement;
    var tag = element.tagName.toLowerCase();
    if (tag === 'a' || tag === 'area') {
      // this is link
      if (element.host === location.host &&
        element.protocol === location.protocol &&
        element.pathname + element.search !== location.pathname + location.search) {
        // this is local & another page
        var xhr = WeRB.getXHR();
        if (xhr) {
          var msgbox = WeRB.showMessage(WeRB.msgWait, 0, 'progress');
          var link = element.href.toString();
          if (element.search === '') {
            link += '?xhr=1';
          } else {
            link += '&xhr=1';
          }
          xhr.open('GET', link, true);

          xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
              if (xhr.status === 200) {
                if (WeRB.updatePage(xhr.responseText)) {
                  if (window.history && history.pushState) {
                    history.pushState(null, null, element.href);
                  }
                }
              } else {
                // TODO: check 30x statuses
                console.log({
                  error : {
                    func : 'clickHandler()',
                    error : xhr.status,
                    xhr : xhr,
                    element : element
                  }
                });
                location.href = element.href;
              }
              WeRB.hideMessage(msgbox);
            }
          };

          xhr.setRequestHeader('Accept', 'application/json');
          xhr.send();
          if (event.preventDefault) {
            event.preventDefault();
          } else {
            event.returnValue = false;
          }
          return false;
        }
      }
    }
    return true;
  },

  SERVER_INTERVAL : 30 * 1000,

  serverQueue : [],

  serverFlag : false,

  serverHandler : function() {
    if (WeRB.serverQueue.length != 0 && !WeRB.serverFlag) {
      WeRB.serverFlag = true;
      var queue = WeRB.serverQueue;
      WeRB.serverQueue = [];
      var xhr = WeRB.getXHR();
      if (xhr) {
        xhr.open('POST', '/server/?xhr=1', true);

        xhr.onreadystatechange = function () {
          if (xhr.readyState === 4) {
            try {
              if (xhr.status === 200) {
                WeRB.updatePage(xhr.responseText);
              }
            } catch (e) {
              console.log({
                error : {
                  func : 'serverHandler() : onreadystatechange()',
                  error : e
                }
              });
            } finally {
              WeRB.serverFlag = false;
            }
          }
        };

        xhr.setRequestHeader('Accept', 'application/json');
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader('Pragma', 'no-cache');
        xhr.send(WeRB.JSON.stringify(queue));
      }
    }
  }

}

// conditional initialization
if (window.DOMTokenList) {
  // modern browsers
  WeRB.hasClass = function (element, name) {
    return element.classList.contains(name);
  };
  WeRB.addClass = function (element, name) {
    element.classList.add(name);
  };
  WeRB.removeClass = function (element, name) {
    element.classList.remove(name);
  };

} else {
  // old browsers
  var cls = function (name) {
    return new RegExp('(\\s|^)' + name + '(\\s|$)');
  };
  WeRB.hasClass = function (element, name) {
    return !!element.className && cls(name).test(element.className);
  };
  WeRB.addClass = function (element, name) {
    if (element.className) {
      if (!cls(name).test(element.className)) {
        element.className += ' ' + name;
      }
    } else {
      element.className = name;
    }
  };
  WeRB.removeClass = function (element, name) {
    if (element.className) {
      element.className = element.className.replace(cls(name), ' ').replace(/(^\s+)|(\s+$)/g, '');
    }
  };
}

// initialization
WeRB.addEvent(window, 'load', function (){

  // events
  WeRB.addEvent(document.body, 'click', WeRB.clickHandler);

  setInterval(WeRB.serverHandler, WeRB.SERVER_INTERVAL);

});





