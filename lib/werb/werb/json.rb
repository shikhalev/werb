# encoding: utf-8

# This file is a part of WeRB project.
# License: LGPLv3+

require 'set'
require 'time'
require 'werb/errors'
require 'werb/core'

class Object

  class << self

    property :jsonable, :class => Set

    def jsonable
      @jsonable ||= Set.new
      if self.superclass.respond_to? :jsonable
        @jsonable | self.superclass.jsonable
      else
        @jsonable
      end
    end

    def unregularize hash
      object = self.new
      hash.each do |key, value|
        method = "#{key}=".intern
        if object.respond_to? method
          object.send method, value
        end
      end
      object
    end

  end

  def to_json level = 0
    hash = { :__class => self.class.name, :__id => self.object_id }
    hash.merge! self.regularize
    hash.to_json level
  end

  def regularize
    hash = {}
    self.class.jsonable.each do |key|
      hash[key] = self.send key
    end
    hash
  end

end

class Struct < Object

  class << self

    def jsonable
      superclass.jsonable | Set[self.members]
    end

  end

end

class String

  def to_json level = 0
    self.inspect
  end

end

class Symbol

  def to_json level = 0
    self.to_name.inspect
  end

end

class Regexp

  def to_json level = 0
    self.inspect.inspect
  end

end

class Numeric

  def to_json level = 0
    self.to_s
  end

end

class Time

  def to_json level = 0
    self.xmlschema(3).to_json level
  end

end

class Hash

  def to_json level = 0
    if self.empty?
      '{}'
    else
      pairs = []
      self.each do |key, value|
        pairs << "\n" + WeRB::JSON::INDENT * (level + 1) + key.to_json(0) +
            ' : ' + value.to_json(level + 1)
      end
      '{' + pairs.join(', ') + "\n" + WeRB::JSON::INDENT * level + '}'
    end
  end

end

class Array

  def to_json level = 0
    if self.empty?
      '[]'
    else
      items = []
      self.each do |item|
        items << "\n" + WeRB::JSON::INDENT * (level + 1) +
            item.to_json(level + 1)
      end
      '[' + items.join(', ') + "\n" + WeRB::JSON::INDENT * level + ']'
    end
  end

end

class Set

  def to_json level = 0
    self.to_a.to_json level
  end

end

class NilClass

  def to_json level = 0
    'null'
  end

end

class FalseClass

  def to_json level = 0
    'false'
  end

end

class TrueClass

  def to_json level = 0
    'true'
  end

end

module WeRB

  module JSON

    INDENT = '  '

    TIME_MASK = %r{
      ^
      \d{4}\-\d{2}\-\d{2}           # Date
      T\d{2}:\d{2}:\d{2}(\.\d{3})?  # Time
      ([+\-]\d{2}:\d{2}|Z)          # Zone
      $
    }x

    REGEXP_MASK = /^\/.*\/[giomxnesu]*$/

    class << self

      def stringify value
        value.to_json 0
      end

      def parse json
        j = pre json

        # @api ignore
        def method_missing name, *args
          case name
          when :null, :undefined
            nil
          else
            name.intern
          end
        end

        h = sandbox { eval j }
        undef method_missing
        post h
      end

      # @api ignore
      def pre json
        json.gsub('&', '&amp;').gsub('=>', '&ergo;').gsub(':', '=>')
      end

      # @api ignore
      def post object
        case object
        when Array
          return object.map { |o| post o }
        when Hash
          hash = {}
          object.each do |key, value|
            key = post key
            if String === key
              hash[key.to_key] = post value
            else
              hash[key] = post value
            end
          end
          if hash[:__class]
            cls = eval hash[:__class]
            return cls.unregularize(hash)
          elsif hash[:__id]
            object = ObjectSpace._id2ref hash[:__id]
            hash.each do |key, value|
              method = "#{key}=".intern
              if object.respond_to? method
                object.send method, value
              end
            end
            return object
          end
          return hash
        when String
          str = object.gsub('=>', ':').gsub('&ergo;', '=>').gsub('&amp;', '&')
          case str
          when TIME_MASK
            return Time.parse(str)
          when REGEXP_MASK
            return eval(str)
          else
            return str
          end
        else
          return object
        end
      end

      private :pre, :post

    end

  end

end
