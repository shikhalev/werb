# encoding: utf-8

# This file is a part of WeRB project.
# License: LGPLv3+

require 'werb/core'

#
module WeRB

  class Error < StandardError

    HTTP = {
      400 => 'Bad Request',
      401 => 'Unauthorized',
      402 => 'Payment Required',
      403 => 'Forbidden',
      404 => 'Not Found',
      405 => 'Method Not Allowed',
      406 => 'Not Acceptable',
      407 => 'Proxy Authentication Required',
      408 => 'Request Timeout',
      409 => 'Conflict',
      410 => 'Gone',
      411 => 'Length Required',
      412 => 'Precondition Failed',
      413 => 'Request Entity Too Large',
      414 => 'Request-URL Too Long',
      415 => 'Unsupported Media Type',
      416 => 'Requested Range Not Satisfiable',
      417 => 'Expectation Failed',
      422 => 'Unprocessable Entity',
      423 => 'Locked',
      424 => 'Failed Dependency',
      425 => 'Unordered Collection',
      426 => 'Upgrade Required',
      449 => 'Retry With',
      456 => 'Unrecoverable Error',
      500 => 'Internal Server Error',
      501 => 'Not Implemented',
      502 => 'Bad Gateway',
      503 => 'Service Unavailable',
      504 => 'Gateway Timeout',
      505 => 'HTTP Version Not Supported',
      506 => 'Variant Also Negotiates',
      507 => 'Insufficient Storage',
      510 => 'Not Extended'
    }

    class << self

      property :status, :class => Integer, :default => 500

    end

    property :headers, :class => Hash

    def initialize headers = {}
      @headers = headers
    end

    class Client < Error
      status! 400
    end

    class Unauthorized < Client
      status! 401
    end

    class Forbidden < Client
      status! 403
    end

    class NotFound < Client
      status! 404
    end

    class Server < Error
      status! 500
    end

    class NotImplemented < Server
      status! 501
    end

  end

end
