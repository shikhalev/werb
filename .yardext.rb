# encoding: utf-8

require 'yard'

class PropertyHandler < YARD::Handlers::Ruby::AttributeHandler

  handles method_call(:property)
  handles method_call(:option)
  namespace_only

  def process
    return if statement.type == :var_ref || statement.type == :vcall
    params = statement.parameters(false).dup
    cls = nil
    case statement.method_name(true)
    when :property
      vvv = '@%s'
      grp = 'Properties'
    when :option
      vvv = '@options[:%s]'
      grp = 'Options'
    end
    while params[-1].type == :list
      a = params.pop.source.scan(/(?::class\s*=>|class:)\s*([\w:]+)/)[0]
      cls = a[0] if a
    end
    validated_attribute_names(params).each do |name|
      var = vvv % name
      namespace.attributes[scope][name] ||= SymbolHash[:read => nil,
                                                       :write => nil,
                                                       :setup => nil]
      { :read => name, :write => "#{name}=",
        :setup => "#{name}!" }.each do |type, meth|
        o = MethodObject.new(namespace, meth, scope)
        case type
        when :write
          par = [['value']]
          sgn = "def #{meth} value"
          src = "#{sgn}\n  #{var} = value\nend"
          doc = "Sets the attribute #{name} value."
          if cls
            doc += "\n@param [#{cls}] value"
            doc += "\n@return [#{cls}]"
          end
        when :read
          sgn = "def #{name}"
          src = "#{sgn}\n  #{var}\nend"
          doc = "Returns the attribute #{name} value."
          if cls
            doc += "\n@return [#{cls}]"
          end
        when :setup
          par = [['value']]
          sgn = "def #{name}! value"
          src = "#{sgn}\n  #{var} << value # abstract pseudocode...\nend"
          doc = "Merge the attribute #{name} value"
          if cls
            doc += "\n@param [#{cls}] value"
            doc += "\n@return [#{cls}]"
          end
        end
        o.parameters = par if par
        o.signature ||= sgn
        o.source ||= src
        register(o)
        o.docstring = doc if o.docstring.blank?(false)
        if type != :setup
          o.group = (scope == :class) ? "Class #{grp}" : "Instance #{grp}"
        end
        namespace.attributes[scope][name][type] = o
      end
    end
  end

end

class ComponentHandler < YARD::Handlers::Ruby::ClassHandler

  handles :class

  @@items = []

  def process
    name = self.statement[0].source
    object = ClassObject.new namespace, name
    if object.superclass == YARD::Registry.resolve(:root, 'WeRB::Component') ||
        @@items.include?(object.superclass)
      @@items << object
      m = MethodObject.new YARD::Registry.resolve(:root, 'WeRB::Item'), object.name.downcase
      m.explicit = false
      m.visibility = :private
      m.parameters = [['id'], ['opts = {}'], ['&block']]
      m.docstring = "Create new #{object.name}.\n" +
          "@yield Inner content.\n" +
          "@return [#{object.name}]\n" +
          "@param [Symbol] id identifier of component\n" +
          "@param [Hash] opts parameter list for {#{object.name}} constructor" +
          " without owner."
      YARD::Registry.register m
    end
  end

end